export interface UserLogged {
    createdAt: string,
    email: string,
    faculty: string,
    fname: string,
    role:string,
    id: number,
    lname: string,
    password: string,
    specialization: string,
    token: string,
    updatedAt: string,
    year_study: string,
    isLoggedIn: boolean,
}

export interface UsersToDisplay {
    name: string,
    faculty: string,
    email: string,
    contact: boolean,
    assigned: boolean
}