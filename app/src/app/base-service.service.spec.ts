import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { BaseService } from './base-service.service';

describe('BaseService', () => {
  let service: BaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientModule],
      providers: []
    });
    service = TestBed.inject(BaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('authenticate should return a true connection', () => {
    const userData = {
      email: 'mircea.dacian@student.upt.ro',
      password: '123456',
    }
    service.authenticate(userData);
    service.userData = {
      createdAt: "test",
      email: "mircea.dacian@student.upt.ro",
      faculty: "string",
      fname: "test",
      role: "test",
      id: 1,
      lname: "test",
      password: "test",
      specialization: "test",
      token: "test",
      updatedAt: "test",
      year_study: "test",
      isLoggedIn: true,

    }
    expect(service.userData['isLoggedIn']).toBe(true);
  });
 
});
