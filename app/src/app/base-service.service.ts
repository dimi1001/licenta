import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UsersToDisplay, UserLogged } from './utils/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  public isLoggedIn: boolean;
  public userData: UserLogged;
  public students: UsersToDisplay[];
  constructor(private http: HttpClient) {
    this.isLoggedIn = false;
  }

  public authenticate(userData) {
    this.http.post<any>('http://localhost:4000/users/authenticate', userData).subscribe(data => {
      if (data?.email === userData) {
        this.userData = data;
        this.userData['isLoggedIn'] = true;
      }
    })
  }

}
