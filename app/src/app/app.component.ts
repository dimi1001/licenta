import { Component, SimpleChanges } from '@angular/core';
import { BaseService } from './base-service.service';
import { UserLogged } from './utils/interfaces/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'Licenta UPT';
  public loginUserName: string;
  public loginPassword: string;
  public error: string;
  public submitted: boolean;
    constructor(private baseService: BaseService) {
    this.loginUserName = '';
    this.loginPassword = '';
    this.error = '';
  }

  ngOnInit() {
    
  }
ngOnChanges(changes: SimpleChanges){
  
}
  public get user(): UserLogged {
    return this.baseService.userData;
  }

  
  public login(event?: any): void {
    const dataLogin = {
      email: this.loginUserName,
      password: this.loginPassword,
    }
    event.preventDefault();
    this.baseService.authenticate(dataLogin);

  }

}
