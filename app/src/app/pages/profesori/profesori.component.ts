import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { BaseService } from '../../base-service.service';

@Component({
  selector: 'app-profesori',
  templateUrl: './profesori.component.html',
  styleUrls: ['./profesori.component.scss']
})
export class ProfesoriComponent implements OnInit {

  public userData= {
    createdAt: "string",
    email: "string",
    faculty: "string",
    fname: "string",
    role:"string",
    id: 3,
    lname: "string",
    password: "string",
    specialization: "string",
    token: "string",
    updatedAt: "string",
    year_study: "string",
    isLoggedIn: true,
  }; 
  public items: any; 
  public isLoggedIn= true;
  constructor(private baseService: BaseService) { 
/*     this.userData = this.baseService.userData;
    this.items = this.baseService.students.filter(el => el.assigned === false); */
  }

  ngOnInit(): void {
  }
}