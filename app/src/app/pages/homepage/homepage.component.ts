import { Component, Input, OnInit, Output, SimpleChange } from '@angular/core';
import { BaseService } from '../../base-service.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  public loginUserName: string;
  public loginPassword: string;
  public error: string;
  public userData = {
    isLoggedIn:true,
  };
  public isLoggedIn = true;
  constructor(private baseService: BaseService) {
  
  }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChange): void {

  }
  public get loginStatus(): boolean {
    return this.baseService.isLoggedIn;
  }
  public get user() {
    return this.baseService.userData;
  }
}
