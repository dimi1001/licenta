import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/app/base-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    public userData: any;
    public editMode: boolean;

  constructor(private baseService: BaseService) {
/*       this.userData = this.baseService.userData;
      this.editMode = false; */
   }

  ngOnInit(): void {
  }

  public contact(teacher): void{
     console.log('Teacher email', teacher);
  }

  public editData(): void{
    this.editMode = !this.editMode;
  }
}
